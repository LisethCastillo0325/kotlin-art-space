package com.example.artspace

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.annotation.ColorRes
import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Add
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material.icons.filled.ArrowForward
import androidx.compose.material.icons.filled.Refresh
import androidx.compose.material.icons.materialIcon
import androidx.compose.material3.BottomAppBar
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.FloatingActionButton
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.IconButtonDefaults
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.OutlinedButton
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.material3.TextButton
import androidx.compose.material3.TopAppBar
import androidx.compose.material3.TopAppBarDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.artspace.ui.theme.ArtSpaceTheme

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            ArtSpaceTheme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    ArtSpaceApp()
                }
            }
        }
    }
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun ArtSpaceApp(modifier: Modifier = Modifier) {

    var currentImage by remember { mutableStateOf(1) }
    val myMap: MutableMap<String, Int?> = mutableMapOf(
        "img" to null,
        "title" to null,
        "actor" to null,
        "year" to null
    )
    var totalWorks = 10
    when (currentImage){
        1 -> {
            myMap["img"] = R.drawable.bruja_escarlata
            myMap["title"] = R.string.bruja_escarlata
            myMap["actor"] = R.string.bruja_escarlata_actor
            myMap["year"] = R.string.bruja_escarlata_year
        }
        2 -> {
            myMap["img"] = R.drawable.capitana_marvel
            myMap["title"] = R.string.capitana_marvel
            myMap["actor"] = R.string.capitana_marvel_actor
            myMap["year"] = R.string.capitana_marvel_year
        }
        3 -> {
            myMap["img"] = R.drawable.gamora
            myMap["title"] = R.string.gamora
            myMap["actor"] = R.string.gamora_actor
            myMap["year"] = R.string.gamora_year
        }
        4 -> {
            myMap["img"] = R.drawable.la_avispa
            myMap["title"] = R.string.la_avispa
            myMap["actor"] = R.string.la_avispa_actor
            myMap["year"] = R.string.la_avispa_year
        }
        5 -> {
            myMap["img"] = R.drawable.mantis
            myMap["title"] = R.string.mantis
            myMap["actor"] = R.string.mantis_actor
            myMap["year"] = R.string.mantis_yer
        }
        6 -> {
            myMap["img"] = R.drawable.nebula
            myMap["title"] = R.string.nebula
            myMap["actor"] = R.string.nebula_actor
            myMap["year"] = R.string.nebula_year
        }
        7 -> {
            myMap["img"] = R.drawable.okoye
            myMap["title"] = R.string.okoye
            myMap["actor"] = R.string.okoye_actor
            myMap["year"] = R.string.okoye_year
        }
        8 -> {
            myMap["img"] = R.drawable.peggy
            myMap["title"] = R.string.peggy
            myMap["actor"] = R.string.peggy_actor
            myMap["year"] = R.string.peggy_year
        }
        9 -> {
            myMap["img"] = R.drawable.shuri
            myMap["title"] = R.string.shuri
            myMap["actor"] = R.string.shuri_actor
            myMap["year"] = R.string.shuri_year
        }
        else -> {
            myMap["img"] = R.drawable.viuda_negra
            myMap["title"] = R.string.viuda_negra
            myMap["actor"] = R.string.viuda_negra_actor
            myMap["year"] = R.string.viuda_negra_year
        }
    }

    Scaffold(
        topBar = {
            TopAppBar(
                colors = TopAppBarDefaults.largeTopAppBarColors(
                    MaterialTheme.colorScheme.primary
                ),
                title = {
                    Text(
                        text = stringResource(R.string.art_space_liseth_castillo),
                        fontSize = 18.sp,
                        color = colorResource(id = R.color.white),
                    )
                }
            )
        },
        bottomBar = {
            BottomAppBar(
                containerColor = MaterialTheme.colorScheme.primaryContainer,
                contentColor = colorResource(id = R.color.pink_900),
            ) {
                Row(
                    modifier = modifier
                        .padding(horizontal = 2.dp)
                        .fillMaxWidth(),
                    horizontalArrangement = Arrangement.SpaceBetween
                ) {
                    ButtonActions(
                        label = R.string.previous,
                        icon = Icons.Default.ArrowBack,
                        onClick = {
                            if (currentImage <= 1) currentImage = 1
                            else currentImage -= 1
                        }
                    )
                    ButtonActions(
                        label = R.string.next,
                        icon = Icons.Default.ArrowForward,
                        onClick = { if (totalWorks > currentImage) currentImage += 1 },
                    )
                }
            }
        },
        floatingActionButton = {
            FloatingActionButton(onClick = {currentImage = 1}) {
                Icon(Icons.Default.Refresh, contentDescription = "Reset")
            }
        }
    ) { innerPadding ->
        Column(
            modifier = Modifier
                .padding(innerPadding),
            verticalArrangement = Arrangement.spacedBy(16.dp),
        ) {
            Column(
                modifier = modifier.fillMaxWidth(),
                horizontalAlignment = Alignment.CenterHorizontally,
            ) {
                ArtworkImage(
                    currentArtwork = myMap["img"]!!.toInt(),
                )
                Spacer(
                    modifier = modifier.size(16.dp)
                )
                ArtworkTitle(
                    title = myMap["title"]!!.toInt(),
                    year = myMap["year"]!!.toInt(),
                    actor = myMap["actor"]!!.toInt(),
                )
                Spacer(
                    modifier = modifier.size(25.dp)
                )
            }
        }
    }
}

@Composable
fun ArtworkImage(
    @DrawableRes currentArtwork: Int,
    modifier: Modifier = Modifier,
) {
    Image(
        painter = painterResource(id = currentArtwork),
        contentDescription = null,
        modifier = modifier
            .fillMaxWidth()
            .padding(top = 30.dp, start = 10.dp, end = 10.dp)
            .clip(RoundedCornerShape(16.dp)),
        contentScale = ContentScale.FillWidth,
    )
}

@Composable
fun ArtworkTitle(
    @StringRes title: Int,
    @StringRes year: Int,
    @StringRes actor: Int,
) {
    Column(
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Text(
            text = stringResource(id = title),
            fontWeight = FontWeight.Bold,
            color = MaterialTheme.colorScheme.primary,
            fontSize = 32.sp,
        )
        Text(
            text = stringResource(id = actor),
            fontWeight = FontWeight.Bold,
            color = MaterialTheme.colorScheme.secondary,
            fontSize = 18.sp,
        )
        Text(
            text = stringResource(id = year),
            fontWeight = FontWeight.Medium,
            color = MaterialTheme.colorScheme.tertiary,
            fontSize = 16.sp,
        )
    }
}

@Composable
fun ButtonActions(
    @StringRes label: Int,
    icon: ImageVector,
    onClick: () -> Unit,
    modifier: Modifier = Modifier
) {
    TextButton(
        onClick = onClick,
        modifier = modifier
    ) {
        Icon(
            imageVector = icon,
            contentDescription = stringResource(label),
        )
    }
}

@Preview(showBackground = true)
@Composable
fun DefaultPreview() {
    ArtSpaceTheme {
        ArtSpaceApp()
    }
}

